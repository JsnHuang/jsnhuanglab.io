---
title: 自製小小Hexo主題
date: 2019-04-06 01:48:46
tags:
- hexo
- theme
---
真的很難克服一定要找麻煩的強迫症，還是想試著自已動手作一個小小的hexo主題。

開始之前還是要先介紹作業環境，模版引擎選擇EJS，它主要語法是JS，而且也沒有太大幅度變更HTML的基本樣式。CSS預處理使用的是stylus。EJS、stylus都是HEXO內建的語法，不必另外下載插件。同樣是以我自已的網站為例。

## 資料結構
在JAS0NHUANG的`themes`資料夾創建一個主題資料夾
```
mkdir ./themes/minimini
cd ./themes/minimini
```

建構必要的文件與資料夾，資料結構會長這樣：
```
.
|-- _config.yml
|-- layout // .ejs模版資料夾
    |-- _partial // 區塊模版資料夾
|-- source // 原檔資料夾，如:CSS、Scripts...等檔案
|-- language // 主題翻譯
```

## 主要EJS模版

