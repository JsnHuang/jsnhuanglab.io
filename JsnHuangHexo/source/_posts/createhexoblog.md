---
title: 用HEXO建立個人網站 
date: 2019-03-30 12:49:04
tags: 
	- Hexo
	- blog
---
python學一學，Linux學一學，**筆記**又開始到處亂寫……
想說還是回頭把BLOG弄好，有個固定的**腦容量擴充平台**，順便分享學到的知識，也算滿足一點虛榮吧!

## HEXO
試過好多種方案，總有每個輪子都要自己造的強迫症，學了點HTML、CSS,看了看Javascript、PHP、SQL，試著用NodeJS、Express重頭搞一頓，全沒真的學會，也在Github上弄了個醜不忍睹的站(不能看，有夠噁)，最後，還是EMAIL、手機、紙張到處亂記。這一次是認真的了，一定要弄好一個**簡單、不醜的小站**，那這次選擇怎麼搞呢?

<!--more-->

看到網路神人[谢益辉](https://yihui.name) (真的神，大家快去拜，雖然某些觀點不太能認同，各位看倌自行判斷就好)的BLOG，版面簡潔，內容豐富，用HUGO做的，所以在LINUX上裝了HUGO加上神人自製主題hugo-ivy，不錯看，但是，對猴子來說設定不很方便，而且又是用GO語言寫的，完全不懂；還是決定用回熟悉的NodeJs，眾大神說Hexo速度慢，但是猴子的小小小網站也沒啥好快的，慢慢來就好，網路上相關文章、教學**感覺**也也比較好找，就決定是它啦!

## 開始建置網站 
系統環境是raspbery pi上面的raspbian，主要透過SSH連線到pi上面操作，使用的文字編輯器是vim，nodejs是官網上下載的最新版本(11.13.0)，網站命名為JAS0NHUANG。
### 本地電腦
### 安裝必要軟體 
安裝Nodejs、npm、git，安裝方式就不新手教學了。
安裝hexo環境
```
sudo npm install -g hexo-cli //安裝hexo-cli(command line interface)到globle
```

### 初始化Hexo資料夾
```
hexo init ./JAS0NHUANG
cd ./JAS0NHUANG
npm install
```

資料夾結構長這樣(只列出**我認為**重要的檔案，git或nodejs相關檔案沒有列出)
```
.
├── _config.yml //設定檔
├── scaffolds //鷹架資料夾，修改md檔以更改頁面內容架構
    ├── draft.md
    ├── page.md
    └── post.md
├── source //文章原始檔資料夾
    └── _posts //寫文章工作資料夾
├── themes //主題資料夾
    └── landscape //Hexo預設的主題
```

### 基本設定 
修改`_config.yml`檔
翻翻網路還發現HEXO是台灣人寫的，向tommy351致敬
```
title: JAS0N HUANG
author: JAS0N HUANG
//先設定這兩項，其它的之後再設定
```
`hexo server` 本地電腦試運行網站

### 套用其它主題
Hexo有一個預設主題landscape,其實也還不錯了，如果想套用其它主題的話，只要導入主題程式庫到themes資料夾，然後修改`_config.yml`裡面的`theme`選項就好，具體作法如下：
```
git submodule add https://github.com/THEMEAUTHOR/THEME.git themes/THEME
//把THEMEAUTHOR換成主題作者的Github帳號，THEME改為主題的程式庫名稱
```

修改`_config.yml`檔
```
theme: THEME
```

覺得UTONE主題看起來簡單，也有點像ivy的外觀，試了一下，還是感覺……
自已弄個小輪胎總行了吧!?

看倌們如果對現成的主題已經很滿意，直接套用一個主題，甚至使用預設的主題就可以發佈到網路上了。(詳見：如何在網路上發佈Hexo網站)
