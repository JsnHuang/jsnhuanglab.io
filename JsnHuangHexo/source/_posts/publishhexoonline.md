---
title: 如何在網路上發佈Hexo網站 
date: 2019-04-03 12:49:04
tags: 
- Hexo
- blog
- github
---
決定好主題，寫好一篇文章，最後一步就是把網站發佈到網路上了，範例中的github帳號為JAS0NHUANG，git page網址是JAS0NHUANG.github.io.git

### 設定並推送至遠端程式庫(remote repository)(以github為例)
先把本地端git程式庫初始化(init)，加入(add)檔案至程式庫,提交(commit)
```
git init
git add *
git commit -m "init"
```

設定遠端程式庫位址
```
git remote add origin git@github.com:jas0nhuang/jas0nhuang.github.io.git
```

建立SSH金鑰
```
ssh-keygen
```
金鑰檔的位置只能用完整路徑，不能用`~/`代替`/home/USERNAME/`,所以通常金鑰檔路徑會是`/home/USERNAME/.ssh/JAS0NHUANG_rsa`，請自行替換USERNAME及JAS0NHUANG
可選擇是否要設定密碼

將新建金鑰設定為此程式庫使用
```
ssh-agent -s //啟動SSH-AGENT
ssh-add ~/.ssh/JAS0NHUANG_rsa
```

顯示公用金鑰
```
cat ~/.ssh/JAS0NHUANG_rsa.pub
```

複製，並加入到github
```
https://github.com/settings/ssh/new
```

很重要的一步，在`_config.yml`檔案裡設定部署方式以及遠端程式庫位址
```
deploy:
type: git
repo: git@github.com:jas0nhuang/jas0nhuang.github.io.git
```

安裝hexo-deployer-git
```
npm instal hexo-deployer-git --save //save選項會把套件加入package.json檔案的node依賴關係中
```

然後就可以用Hexo部署了
```
hexo deploy
```
